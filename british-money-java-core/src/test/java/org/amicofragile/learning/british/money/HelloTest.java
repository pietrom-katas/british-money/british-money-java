package org.amicofragile.learning.british.money;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class HelloTest {
    @Test
    public void sayHelloWorld() {
        assertThat(new Hello().sayHello(), is(equalTo("Hello, World!")));
    }
}
